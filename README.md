# Bài tập Git work flow

***Git work flow***
- Tổng quan về git
- Các câu lệnh cơ bản
- *Thực hiện bởi Phạm Luận*

## Liên kết
- [Tài liệu git](https://git-scm.com/doc)
- [Mẫu Readme.md](https://github.com/colombo-group/trainee_2018/blob/master/TEMPLATE%20READMES.MD)!
- [Conventional commits](https://www.conventionalcommits.org/en/v1.0.0/)
- [Basic formatting syntax](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)

## Thực hành

- Tạo repository mới
- Chia branch
- Remote và up code
- Tạo các commit theo chuẩn [Conventional commits](https://www.conventionalcommits.org/en/v1.0.0/)
- Merge branch
- Viết file README.md

## Kiến thức nắm được
- Lí thuyết cơ bản về git
- Nắm được tổng quan về git work flow
- Biết cách xử lý conflict

***Git là gì? Tại sao phải sử dụng git?***
- Git là dịch vụ lưu trữ source code, hệ thống quản lí phiên bản phân tán
- Hệ thống giúp lưu trữ mọi thay đổi của source code
- Hỗ trợ nhiều nguời làm việc cùng lúc, biết được ai là người thay đổi code
- Kiểm soát phiên bản tốt, revert các thay đổi, đưa code về version cũ hơn, không lo mất code
- Toàn bộ code và lịch sử thay đổi được lưu trữ trên máy người dùng

***Trách nhiệm khi làm việc nhóm***
- Mỗi thành viên nên phát triển từng tính năng trên các nhánh riêng biệt
- Nên pull code trước khi tạo nhánh mới để đảm bảo code là mới nhất
- Nên commit thường xuyên
- Ai gây ra conflict thì người đó sẽ chủ động giải quyết trước

## Convetional commit
***Tại sao nên sử dụng Conventional commit?***
- Mọi người trong cùng 1 dự án cũng như người mới có thể hình dung được bao quát được nội dung code thay đổi làm công việc chính là gì, tiện cho việc review đánh giá cũng như tìm kiếm
- Lưu giữ đầy đủ thông tin có thể tìm kiếm và kiểm chứng được các thay đổi
- Dễ dàng phân loại các commit
- Giúp mọi người trong dự án có thói quen phân nhỏ các commit theo mục đích hơn là tập trung tất cả và một commit chung

***Cấu trúc của một commit conventional***
- ```<type>[optional scope]: <description> [optional body] [optional footer]```
- Các thành phần type, description là bắt buộc cần có trong commit message, option là tùy chọn
- Type là các từ khóa để phân loại các commit

    - feat: thêm một tính năng mới
    - fix: thực hiện fix bug cho hệ thống
    - refactor: sửa code nhưng không fix bug cũng không thêm feature hoặc đôi khi bug cũng được fix từ việc refactor, Tối ưu source code, có thể liên quan logic… Ví dụ như xoá code thừa, tối giản code, đổi tên biến…
    - docs: Những thay đổi liên quan đến sửa đổi document
    - style: Những thay đổi không làm thay đổi ý nghĩa của code như căn hàng, xuồng dòng ở cuối file, thay đổi css,...
    - perf: thực hiện cải thiện hiệu năng của dự án (VD: loại bỏ duplicate query, ...)
    - test: Thêm hoặc sửa các testcase trong hệ thống.
    - chore: Những sửa đổi nhỏ nhặt không liên quan tới code, như thay đổi text,...
    - vendor: thực hiện cập nhật phiên bản cho các packages, dependencies mà dự án đang sử dụng
    - BREAKING CHANGE: Nhưng commit mới footer là BREAKING CHANGE thể hiện những thay đổi gây ảnh hướng lớn đến source code ví dụ thay đổi kiểu dữ liệu, cách lấy dữ liệu

- Scope: 

- Tùy chọn, thể hiện việc thay đổi code sẽ có phạm vi ảnh hưởng đến đâu
- Đứng ngay sau <type> và được đặt trong dấu ngoặc đơn
- Ví dụ: ```feat(lang): add us language ```

- description: mô tả ngắn gọn về những thay đổi trong lần commit này. ví dụ: ```feat: add authentication```
- body: tùy chọn, là mô tả dài và chi tiết hơn, cần thiết khi description chưa thể nói rõ hết được.
- footer: tùy chọn, chứa các thông thông tin mở rộng của pull request ví dụ như là danh sách người review, link/id của các pull request có liên quan.
- BREAKING CHANGE: 

    - Đánh dấu sự thay đổi trong code làm thay đổi lớn khiến cho nó không còn tương thích với phiên bản trước nữa 
    - Có 2 cách để đánh dấu một BREAKING CHANGE: đặt từ khóa BREAKING CHANGE vào đầu footer hoặc ghi dấu ! ngay sau type/scope hoặc sử dụng cả 2.

## Basic formatting syntax

- Văn bản
  - Tiêu đề: sử dụng từ 1 đến 6 dấu `#` để thể hiện độ lớn của tiêu đề giảm dần.
    Khi sử dụng nhiều tiêu đề (>=2) git sẽ tự động tạo ra mục lục cho tài liệu
  - In đậm: sử dụng `** **` hoặc `__  __`
  - in nghiêng sử dụng: `* *` hoặc `_ _`
  - in đậm và nghiêng: sử dụng `***  ***`
  - chữ gạch ngang: sử dụng `~~ ~~`
  - chỉ số dưới: `<sub></sub>`
  - chỉ số trên: `<sup></sup>`
  - chèn liên kiết: sử dụng `[Văn bản hiển thị](đường dẫn)`
  - chèn hình ảnh: sử dụng  `![văn bản thay thế nếu ảnh lỗi](đường dẫn)`
  - danh sách: sử dụng `-` hoặc `*` hoặc số thập phân cho danh sách có đánh số

> Trích dẫn văn bản: sử dụng  `>`
- Tạo checkbox sử dụng `- [ ]` cho option chưa được đánh dấu và `- [x]`cho option đã được đánh dấu
- [ ] option chưa được đánh dấu
- [x] option đã được đánh dấu
- Comment sử dụng `<!-- nội dung comment -->`
<!-- comment -->

## Advance
***Xử lí conflict***
- khi xảy ra conflict, git sẽ thêm file bị conflict các từ khóa:
  - Nội dung phía sau dòng `<<<<<<< HEAD` đến `=======` là nội dung tồn tại trong nhánh hiện tại
  - Nội dung phía sau dòng `=======` đến `>>>>>>> branch_name` là nội dung bị conflict
  - Nội dung phía sau dòng `>>>>>>> branch_name` là nội dung có trong nhánh chuẩn bị được merge vào nhánh hiện tại
- Cách giải quyết: chỉnh sửa trực tiếp trong file có nội dung bị conflict

***Revert commit***
- quay trở về commit cũ mà không làm mất các commit, thay vào đó sẽ tạo commit mới, có nội dung
  giống hệt với commit mà ta muốn quay lại
- mở lịch sử của repository bằng lệnh `git log`, tìm id của commit muốn quay lại
- dùng lệnh `git revert <id-commit>` để thực hiện việc quay lại commit cũ

***Reset commit***
- Tương tự như revert, lệnh reset cũng giúp ta quay lại một commit nào đó, tuy nhiên nó sẽ đồng thời xóa lịch sử các commit mới
- mở lịch sử của repository bằng lệnh `git log`, tìm id của commit muốn quay lại
- dùng lệnh `Git reset --hard <id-commit>` để thực hiện việc quay lại commit cũ

***Cherry pick***
- git cherry pick là một cách để đưa thay đổi của một commit trên một nhánh nào đó áp dụng vào nhánh hiện tại
- dùng lệnh `git cherry-pick <branch_name>` để đưa commit cuối cùng của branch khác vào branch hiện tại
- hoặc sử dụng commit_id: `git cherry-pick commit_id1` để đưa commit có id là commit_id1 vào branch hiện tại
- `git cherry-pick commit_id1 commit_id3` để đưa commit có id là commit_id1 và commit_id3 vào branch hiện tại
- `git cherry-pick commit_id1...commit_id6` để đưa commit có id là commit_id2 đến commit_id6 vào branch hiện tại

***Squash commit***
- Sử dụng trong trường hợp cần gộp nhiều commit lại thành một commit
- mở git log bằng lệnh `git log`, tìm và coppy id của commit nằm trước tất cả các commit cần squash
- dùng lệnh `git rebase -i commit_id`, cửa sổ chỉnh sửa sẽ hiện ra danh sách các commit cần squash
- giữ nguyên commit trên cùng là pick, và sửa các commit phía sau thành squash
- tiến hành lưu lại file và đặt tên cho commit mới


